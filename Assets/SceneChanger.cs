using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour
{
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.RightArrow))
        {
            int sceneIndex = SceneManager.GetActiveScene().buildIndex + 1;

            Debug.Log(sceneIndex);

            if(sceneIndex > SceneManager.sceneCountInBuildSettings - 1)
            {
                sceneIndex = 0;
            }

            SceneManager.LoadScene(sceneIndex);
        }
        if(Input.GetKeyDown(KeyCode.LeftArrow))
        {
            int sceneIndex = SceneManager.GetActiveScene().buildIndex - 1;

            if(sceneIndex < 0)
            {
                sceneIndex = SceneManager.sceneCountInBuildSettings - 1;
            }

            SceneManager.LoadScene(sceneIndex);
        }
    }
}
