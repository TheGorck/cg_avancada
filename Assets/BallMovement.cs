using UnityEngine;

public class BallMovement : MonoBehaviour
{
    Vector3 movement;

    private void Start() 
    {
        movement = transform.position;
    }

    private void Update()
    {
        movement = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")).normalized;
        movement *= 5f * Time.deltaTime; 

        movement += transform.position;

        movement.x = Mathf.Clamp(movement.x, -1f, 8f);
        movement.z = Mathf.Clamp(movement.z, -3f, 4f);

        transform.position = movement;
    }
}
